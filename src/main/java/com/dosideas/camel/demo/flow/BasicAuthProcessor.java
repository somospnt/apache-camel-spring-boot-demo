package com.dosideas.camel.demo.flow;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class BasicAuthProcessor implements Processor {

    @Value("${com.dosideas.camel.demo.username}")
    private String username;
    @Value("${com.dosideas.camel.demo.password}")
    private String password;

    @Override
    public void process(Exchange exchange) throws Exception {
        System.out.println("--------------------------------");
        String header = exchange.getIn().getHeader("Authorization", String.class);
        header = header.substring(6); //quitar el "Basic "
        String userpass = new String(Base64.getDecoder().decode(header));
        String[] tokens = userpass.split(":");
        for (String token : tokens) {
            System.out.println("Token: " + token);
        }
        if (! (tokens[0].equals(username) && tokens[1].equals(password))) {
            throw new IllegalArgumentException("sin acceso");
        }

    }

}
