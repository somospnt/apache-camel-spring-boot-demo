package com.dosideas.camel.demo.flow;

import com.dosideas.camel.demo.domain.Persona;
import com.dosideas.camel.demo.domain.Personaje;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RestFlowCamel extends RouteBuilder {

    @Autowired
    private BasicAuthProcessor basicAuthProcessor;
    @Autowired
    private ArmarBasicAuthProcessor armarBasicAuthProcessor;

    @Override
    public void configure() throws Exception {
        restConfiguration().port(8080).bindingMode(RestBindingMode.json);

//        onException(Exception.class)
//                .handled(true)
//                .to("direct:error");

        rest("/hello")
                .post()
                .consumes("application/json")
                .produces("text/plain")
                .type(Persona.class)
                .to("direct:saludar");

        from("direct:saludar")
                .onException(Exception.class)
                    .maximumRedeliveries(3).maximumRedeliveryDelay(2000)
                    .handled(true)
                    .to("direct:error")
                    .end()
                .process(basicAuthProcessor)
                .log("${body}")
                .transform().simple("hola, mundo para el ${body.nombre}");

        from("direct:error")
                .log("OCURRIO UN ERROR HORRIBLE!!!!!!!!!!!!!!!!!!!!!!!")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant("403"))
                .transform().simple("No tiene acceso");

        rest("/personaje")
                .get()
                .consumes("application/json")
                .produces("text/plain")
                .to("direct:consumir-post-http");

        // Con HTTP Component
        from("direct:consumir-post-http")
                .process(armarBasicAuthProcessor)
                .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.POST))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .setBody(constant(new Personaje(null, "LALA", "LALA2")))
                .marshal().json()
                .to("log:out")
                .to("http://localhost:8080/personaje" + "?bridgeEndpoint=true")
                .unmarshal().json();



        // Con REST Component
        from("direct:consumir-post-rest")
                .process(armarBasicAuthProcessor)
                .setBody(constant(new Personaje(null, "LALA", "LALA2")))
                .marshal().json(JsonLibrary.Jackson, Personaje.class)
                .to("rest:post:personaje?host=localhost:8080")
                .unmarshal().json();
    }



    // HACER UN CANAL QUE SEA UN GET QUE LE PEGUE A NUESTRO SERVICIO POST QUE YA HICIMOS
    // HACER UN PROCESSOR QUE GENERE LOS DATOS DE AUTENTICACION
}
