package com.dosideas.camel.demo.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AgrupadorService {

    private static final int GROUP_SIZE = 2;

    public static Iterator agrupar(final Iterator list) {
        return new Iterator() {
            @Override
            public boolean hasNext() {
                return list.hasNext();
            }

            @Override
            public Object next() {
                List result = new ArrayList();
                for (int i = 0; i < GROUP_SIZE && list.hasNext(); i++) {
                    result.add(list.next());
                }
                return result.iterator();
            }
        };
    }

}
